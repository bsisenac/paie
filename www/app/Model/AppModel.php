<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
     public function mensagens_erro( $formato_html = true ){
        
        $erros = $this->validationErrors;
        $mensagem = "";
        foreach ($erros as $erro)
            if( is_array( $erro ) )
                foreach ($erro as $string )
                    $mensagem .= " - " . $string . "[quebra_linha][quebra_linha]";
        
        // Remove as quebras de linha no final
        $mensagem = preg_replace( '/(\[quebra_linha\]){2}$/', "", $mensagem );
        
        // Adiciona a quebra de linha no formato HTML ou texto
        if( $formato_html )
            $mensagem = preg_replace( "/(\[quebra_linha\])/", "<br />", $mensagem );
        else
            $mensagem = preg_replace( "/(\[quebra_linha\])/", "\n", $mensagem );
            
       return $mensagem;
    }
}
