<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property Grupo $Grupo
 * @property Comentario $Comentario
 * @property Documento $Documento
 * @property Lixeira $Lixeira
 * @property Log $Log
 * @property Reacao $Reacao
 */
class User extends AppModel {

	public $recursive = 2;

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'password' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'grupo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nome' => array(
			'numeric' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'username' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
			'unique' => array(
	            'rule' => array( 'isUnique' ),
	            'required' => 'create',
	            'message' => 'E-mail já existente. Acesse a opção de "Esqueci minha senha" para recuperar sua senha.',
			),
		),
		'sobrenome' => array(
			'numeric' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'password_2' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
            'compare'    => array(
                'rule'      => array('validar_senha'),
                'message'   => 'As senhas não coincidem.',
                'allowEmpty' => true
            ),
        ),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $displayField = 'fullname';
	public $virtualFields = array( 'fullname' => 'concat( User.id, " - ", User.nome )' );

	// public $actsAs = array('Acl' => array('type' => 'requester'));
    public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['grupo_id'])) {
            $grupoId = $this->data['User']['grupo_id'];
        } else {
            $grupoId = $this->field('grupo_id');
        }
        if (!$grupoId) {
            return null;
        }
        return array('Grupo' => array('id' => $grupoId));
    }
    
    public function criptarSenha( $senha ){

        return BlowfishPasswordHasher::hash( $senha );
    }
   
     public function beforeSave($options = array()) {
    	if( isset( $this->data['User']['password'] ) and $this->data['User']['password'] ){
    		$this->data['User']['password'] = $this->criptarSenha(  $this->data['User']['password'] );
    	};
        return parent::beforeSave( $options );
    	
    }
    
    public function bindNode($user) {
        return array('model' => 'Grupo', 'foreign_key' => $user['User']['grupo_id']);
    }

    public function validar_senha() {
	    return $this->data[ 'User' ]['password'] === $this->data[ 'User' ][ 'password_2' ];
	}
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Grupo' => array(
			'className' => 'Grupo',
			'foreignKey' => 'grupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Escola' => array(
			'className' => 'Escola',
			'foreignKey' => 'escola_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */

	public $hasMany = array(
		'Log' => array(
			'className' => 'Log',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Notificacao' => array(
			'className' => 'Notificacao',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Nota' => array(
			'className' => 'Nota',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
