<?php
App::uses('AppModel', 'Model');
App::import( 'Controller', 'Aros' );
/**
 * Grupo Model
 *
 * @property User $User
 */
class Grupo extends AppModel {

	public $actsAs = array('Acl' => array('type' => 'requester'));

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'grupo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function parentNode() {
        return null;
    }

    // public function afterSave( $created, $options = array( ) ) {
    // 	if( $this->acao_log == "I" ){
    // 		$this->add_permissao_administrador( $this->id );
    // 	};
    // 	return parent::afterSave( $created, $options = array( ) );

    // }
    
    
}
