<?php
App::uses('AppController', 'Controller');
/**
 * Turmas Controller
 *
 * @property Turma $Turma
 * @property PaginatorComponent $Paginator
 */
class TurmasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Turma->recursive = 0;
		$this->set('turmas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Turma->exists($id)) {
			throw new NotFoundException(__('Invalid turma'));
		}
		$options = array('conditions' => array('Turma.' . $this->Turma->primaryKey => $id));
		$this->set('turma', $this->Turma->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Turma->create();
			if ($this->Turma->save($this->request->data)) {
				$this->Session->setFlash(__('The turma has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The turma could not be saved. Please, try again.'));
			}
		}
		$escolas = $this->Turma->Escola->find('list');
		$disciplinas = $this->Turma->Disciplina->find('list');
		$users = $this->Turma->User->find('list');
		$this->set(compact('escolas', 'disciplinas', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Turma->exists($id)) {
			throw new NotFoundException(__('Invalid turma'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Turma->save($this->request->data)) {
				$this->Session->setFlash(__('The turma has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The turma could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Turma.' . $this->Turma->primaryKey => $id));
			$this->request->data = $this->Turma->find('first', $options);
		}
		$escolas = $this->Turma->Escola->find('list');
		$disciplinas = $this->Turma->Disciplina->find('list');
		$users = $this->Turma->User->find('list');
		$this->set(compact('escolas', 'disciplinas', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Turma->id = $id;
		if (!$this->Turma->exists()) {
			throw new NotFoundException(__('Invalid turma'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Turma->delete()) {
			$this->Session->setFlash(__('The turma has been deleted.'));
		} else {
			$this->Session->setFlash(__('The turma could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
