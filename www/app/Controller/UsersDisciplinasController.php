<?php
App::uses('AppController', 'Controller');
/**
 * UsersDisciplinas Controller
 *
 * @property UsersDisciplina $UsersDisciplina
 * @property PaginatorComponent $Paginator
 */
class UsersDisciplinasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UsersDisciplina->recursive = 0;
		$this->set('usersDisciplinas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UsersDisciplina->exists($id)) {
			throw new NotFoundException(__('Invalid users disciplina'));
		}
		$options = array('conditions' => array('UsersDisciplina.' . $this->UsersDisciplina->primaryKey => $id));
		$this->set('usersDisciplina', $this->UsersDisciplina->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UsersDisciplina->create();
			if ($this->UsersDisciplina->save($this->request->data)) {
				$this->Session->setFlash(__('The users disciplina has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users disciplina could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersDisciplina->User->find('list');
		$disciplinas = $this->UsersDisciplina->Disciplina->find('list');
		$this->set(compact('users', 'disciplinas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UsersDisciplina->exists($id)) {
			throw new NotFoundException(__('Invalid users disciplina'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersDisciplina->save($this->request->data)) {
				$this->Session->setFlash(__('The users disciplina has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users disciplina could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersDisciplina.' . $this->UsersDisciplina->primaryKey => $id));
			$this->request->data = $this->UsersDisciplina->find('first', $options);
		}
		$users = $this->UsersDisciplina->User->find('list');
		$disciplinas = $this->UsersDisciplina->Disciplina->find('list');
		$this->set(compact('users', 'disciplinas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UsersDisciplina->id = $id;
		if (!$this->UsersDisciplina->exists()) {
			throw new NotFoundException(__('Invalid users disciplina'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersDisciplina->delete()) {
			$this->Session->setFlash(__('The users disciplina has been deleted.'));
		} else {
			$this->Session->setFlash(__('The users disciplina could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
