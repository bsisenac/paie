<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $uses = array('User');

	public $helpers = array('Html', 'Form', 'Session');

    public $components = array(
        'Auth' => array(
            'authorize' => array(
                
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
            
        ),
        'Session',
    );

    public function message_json_success_app( $json ){
        return $this->message_json_app( $json, true, false, false );
    }
    
    public function message_json_error_app( $json ){
        return $this->message_json_app( $json, false, true, false );
    }
    
    public function message_json_warning_app( $json ){
        return $this->message_json_app( $json, false, false, true );
    }
    
    private function message_json_app( $json, $success, $failed, $warning ){
        if( $success ) $success = "true";
        else $success = "false";
        
        if( $failed ) $failed = "true";
        else $failed = "false";
        
        if( $warning ) $warning = "true";
        else $warning = "false";
            
        $retorno = '{ "success": ' . $success  .
                ', "failed": ' . $failed . 
                ', "warning": ' . $warning . 
                ', "message": ' . $json . '}';
                
        return $retorno;
    }

    public function beforeFilter() {

        $profile = $this->get_profile();
        $this->set('profile',$profile);

         // Actions habilitadas para usuários não logados
        $this->Auth->allow('login', 'logout', 'getAll_app');

        parent::beforeFilter();

    }

    protected function get_profile(){
        if ($this->Session->read('Auth.User')) {
            $currentUser = $this->Session->read('Auth.User');
            if(!empty($currentUser['User']['id'])){
                $id = $currentUser['User']['id'];
            }
            else {
                $id = $currentUser['id'];
            }
            $profile = $this->User->findById($id);
        }else {
            $profile = 0;
        }
        return $profile;
    }

    function getAll_app( $options = null ){
        $this->autoRender = false;
        $modal = $this->modelClass;
        $retorno = $this->{$modal}->find( "all", $options );
        echo $this->message_json_success_app( json_encode( $retorno ) );
    }
}
