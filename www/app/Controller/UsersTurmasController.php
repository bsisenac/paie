<?php
App::uses('AppController', 'Controller');
/**
 * UsersTurmas Controller
 *
 * @property UsersTurma $UsersTurma
 * @property PaginatorComponent $Paginator
 */
class UsersTurmasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UsersTurma->recursive = 0;
		$this->set('usersTurmas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UsersTurma->exists($id)) {
			throw new NotFoundException(__('Invalid users turma'));
		}
		$options = array('conditions' => array('UsersTurma.' . $this->UsersTurma->primaryKey => $id));
		$this->set('usersTurma', $this->UsersTurma->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UsersTurma->create();
			if ($this->UsersTurma->save($this->request->data)) {
				$this->Session->setFlash(__('The users turma has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users turma could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersTurma->User->find('list');
		$turmas = $this->UsersTurma->Turma->find('list');
		$this->set(compact('users', 'turmas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UsersTurma->exists($id)) {
			throw new NotFoundException(__('Invalid users turma'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersTurma->save($this->request->data)) {
				$this->Session->setFlash(__('The users turma has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users turma could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersTurma.' . $this->UsersTurma->primaryKey => $id));
			$this->request->data = $this->UsersTurma->find('first', $options);
		}
		$users = $this->UsersTurma->User->find('list');
		$turmas = $this->UsersTurma->Turma->find('list');
		$this->set(compact('users', 'turmas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UsersTurma->id = $id;
		if (!$this->UsersTurma->exists()) {
			throw new NotFoundException(__('Invalid users turma'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersTurma->delete()) {
			$this->Session->setFlash(__('The users turma has been deleted.'));
		} else {
			$this->Session->setFlash(__('The users turma could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
