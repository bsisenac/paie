<?php
App::uses('AppController', 'Controller');
/**
 * TurmasDisciplinas Controller
 *
 * @property TurmasDisciplina $TurmasDisciplina
 * @property PaginatorComponent $Paginator
 */
class TurmasDisciplinasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TurmasDisciplina->recursive = 0;
		$this->set('turmasDisciplinas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TurmasDisciplina->exists($id)) {
			throw new NotFoundException(__('Invalid turmas disciplina'));
		}
		$options = array('conditions' => array('TurmasDisciplina.' . $this->TurmasDisciplina->primaryKey => $id));
		$this->set('turmasDisciplina', $this->TurmasDisciplina->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TurmasDisciplina->create();
			if ($this->TurmasDisciplina->save($this->request->data)) {
				$this->Session->setFlash(__('The turmas disciplina has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The turmas disciplina could not be saved. Please, try again.'));
			}
		}
		$disciplinas = $this->TurmasDisciplina->Disciplina->find('list');
		$turmas = $this->TurmasDisciplina->Turma->find('list');
		$this->set(compact('disciplinas', 'turmas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->TurmasDisciplina->exists($id)) {
			throw new NotFoundException(__('Invalid turmas disciplina'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TurmasDisciplina->save($this->request->data)) {
				$this->Session->setFlash(__('The turmas disciplina has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The turmas disciplina could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TurmasDisciplina.' . $this->TurmasDisciplina->primaryKey => $id));
			$this->request->data = $this->TurmasDisciplina->find('first', $options);
		}
		$disciplinas = $this->TurmasDisciplina->Disciplina->find('list');
		$turmas = $this->TurmasDisciplina->Turma->find('list');
		$this->set(compact('disciplinas', 'turmas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->TurmasDisciplina->id = $id;
		if (!$this->TurmasDisciplina->exists()) {
			throw new NotFoundException(__('Invalid turmas disciplina'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TurmasDisciplina->delete()) {
			$this->Session->setFlash(__('The turmas disciplina has been deleted.'));
		} else {
			$this->Session->setFlash(__('The turmas disciplina could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
