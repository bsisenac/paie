<?php
App::uses('AppController', 'Controller');
/**
 * Escolas Controller
 *
 * @property Escola $Escola
 * @property PaginatorComponent $Paginator
 */
class EscolasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Escola->recursive = 0;
		$this->set('escolas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		$this->Escola->recursive = 2;
		
		if (!$this->Escola->exists($id)) {
			throw new NotFoundException(__('Invalid escola'));
		}
		$options = array('conditions' => array('Escola.' . $this->Escola->primaryKey => $id));
		$this->set('escola', $this->Escola->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Escola->create();
			if ($this->Escola->save($this->request->data)) {
				$this->Session->setFlash(__('The escola has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The escola could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Escola->exists($id)) {
			throw new NotFoundException(__('Invalid escola'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Escola->save($this->request->data)) {
				$this->Session->setFlash(__('The escola has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The escola could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Escola.' . $this->Escola->primaryKey => $id));
			$this->request->data = $this->Escola->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Escola->id = $id;
		if (!$this->Escola->exists()) {
			throw new NotFoundException(__('Invalid escola'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Escola->delete()) {
			$this->Session->setFlash(__('The escola has been deleted.'));
		} else {
			$this->Session->setFlash(__('The escola could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
