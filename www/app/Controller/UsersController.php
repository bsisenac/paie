<?php
App::uses( "HttpSocket", "Network/Http" );
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    public $components = array( "Paginator" );
/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
        
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $escolas = $this->User->Escola->find('list');
        $grupos = $this->User->Grupo->find('list');
        $this->set(compact('grupos', 'escolas'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
        $escolas = $this->User->Escola->find('list');
        $grupos = $this->User->Grupo->find('list');
        $this->set(compact('grupos', 'escolas'));
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted.'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    
/**
 * login method
 *
 * @throws
 * @param
 * @return void
 */     

    public function login( ) {
       
        
        if ( AuthComponent::user( 'id' ) ) {
            return $this->redirect('/');
        }
                
        if ( $this->request->is('post') ) {

            if ($this->Auth->login()) {
                
                $user = $this->User->findById( AuthComponent::user('id') );
                
                $this->Session->write( 'User', $user );
                return $this->redirect( '/users/' );
            }
            
            $this->Session->setFlash(__( 'Nome de usuário desconhecido ou senha inválida.' ), 'danger');
        }

        if( empty( $this->request->data[ "User" ][ "redirect" ] ) ){
            $this->set( "redirect", Controller::referer() );
        }else{
            $this->set( "redirect", $this->request->data[ "User" ][ "redirect" ] );
        }
        $this->set( "semlogo", true );
    }

    public function login_app( ){
        
         $this->Auth->logout();
         if ( $this->request->is('post') ) {

            if ($this->Auth->login()) {
                
                $user = $this->User->findById( AuthComponent::user('id') );
                $this->set( "user", $this->message_json_success_app( json_encode( $user ) ) );
                
            }else{
                $this->set( "user", $this->message_json_error_app( json_encode( "Senha e/ou usuário inválido" ) ) );

            }
        }

        $this->layout = "ajax";

    }

    public function logout() {

        $this->Session->write( 'User', null );
        $this->redirect($this->Auth->logout());
        
    }

    public function beforefilter(){

        parent::beforeFilter();
        $this->Auth->allow( 'getAll', 'login', 'add', 'logout', 'recuperar_senha', 'login_app', 'upload_app' );

    }

    public function recuperar_senha( ) {
        $this->set( "semlogo", true );
        if( AuthComponent::user('id') ){
            $this->redirect( "/" );
        }
        
        if( $this->request->is( "post" ) ){

            $email = $this->request->data["User"]["username"];
            $condicao['User.username'] = $email;
            $user = $this->User->find( "first", array( 'conditions' => $condicao ) );

            if( ! $user ){
                $this->Session->setFlash( 'E-mail não encontrado, verifique se foi preenchido corretamente.', 'danger' );
                return false;
            }

            $senha = $this->User->gerar_senha();
            $user["User"]["password"] = $this->User->criptarSenha( $senha );
            $user["User"]["expirar_senha"] = $this->data_senha_expirar();
            unset( $user["User"]["validado"] );
            unset( $user["User"]["origem"] );
            $user["User"]["user_id"] = $user["User"]["id"];
            $this->User->Save( $user );

            $this->Session->write( "User", $this->User->findById( $user["User"]["user_id"] ) );
            $this->SapotiEmail->recuperar_senha( $user, $senha );
            $this->Session->setFlash( "Uma nova senha foi enviada para o seu e-mail", "success" );
            $this->redirect( "/users/alterar_senha/" );
        }
    }

    public function alterar_senha( ) {

        $this->layout = "default";
        $user_id = AuthComponent::user('id');

        if( ! $user_id ){
            $this->redirect( "/users/login/" );
        }

        if( $this->request->is( "post" ) ){

            if( $this->request->data["User"]["password"] !== $this->request->data["User"]["password_confirmacao"] )
            {
                $this->Session->setFlash( "As senhas informadas não conferem.", 'danger' );
                return false;
            }

            $user = $this->User->findById( $user_id );

            $user["User"]["password"] = $this->User->criptarSenha( $this->request->data["User"]["password"] );
            $user["User"]["expirar_senha"] = $this->data_senha_expirar();

            unset( $user["User"]["validado"] );
            unset( $user["User"]["origem"] );

            $this->User->Save( $user );
            $this->Session->write( "User", $this->User->findById( $user_id ) );
            $this->SapotiEmail->alteracao_senha( $user );


            $this->redirect( "/" );

        }
    }

    public function data_senha_expirar(){

        $dias = 25;
        $atual = date( "Y-m-d" );
        return date( 'Y-m-d', strtotime( $atual . ' +  ' . $dias . '  day' ) );
    }
    
   public function upload_app($title = null, $text = null){

        // $this->autoRender = false;

        // $title = "teste";
        // $text = "conteúdo de teste";        
        //  $header = array(
        //    "en"    => $title
        //  );
        //  $content = array(
        //   "en"     => $text,
        //   );

        // $fields = array(
        //   'app_id' => "a4a49f5b-1804-4c2a-bab9-958604edbfcf",
        //   'included_segments' => array('All'),
        //   'data' => array("foo" => "bar"),
        //   'headings' => $header,
        //   'contents' => $content
        // );

        // $fields = json_encode($fields);

        // $http = new HttpSocket( array( 'ssl_verify_host'=>false ) );

        //  $request = array(
        //     'header' => array(
        //         'Content-Type' => 'application/json',
        //         'Authorization' => 'Basic NjI1ZDExMDAtMjM0OS00YjBmLTg3YjgtMjYzOGIzYTExOWY2'
        //     ),
        // );

        // $response = $http->post('https://onesignal.com/api/v1/notifications', $fields, $request);
        // var_dump($response);
        $user = $this->User->findById( $this->request->data[ "user_id" ] );
        unset( $user["User"]["password"] );
        
        $user["User"]["url_img"] = $this->request->data["file"];
        $this->User->save( $user );
        $this->message_json_success_app( $user );
        
        // $this->layout = "";
        // $this->autoRender = false;
        // echo $this->render( "login_app" );
        
    }
    
    // public function getAll_app( $options = null ){
    
    // $this->autoRender = false;
    // $modal = $this->modelClass;
    // $retorno = $this->{$modal}->find( "all", $options );

    // $key = array_search('url_img', $retorno);
    // unset($fields[$key]);
        
    // echo $this->message_json_success_app( json_encode( $retorno ) );
           
    // }
}