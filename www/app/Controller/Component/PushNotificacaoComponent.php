<?php
App::uses('Component', 'Controller');

class PushNotificacaoComponent extends Component {

  public function enviarNotificacao( $title, $text ){

    $this->autoRender = false;

     $header = array(
       "en"    => $title
     );
     $content = array(
      "en"     => $text,
      );

    $fields = array(
      'app_id' => "a4a49f5b-1804-4c2a-bab9-958604edbfcf",
      'included_segments' => array('All'),
      'data' => array("foo" => "bar"),
      'headings' => $header,
      'contents' => $content
    );

    $fields = json_encode($fields);

    $http = new HttpSocket( array( 'ssl_verify_host'=>false ) );

     $request = array(
        'header' => array(
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic NjI1ZDExMDAtMjM0OS00YjBmLTg3YjgtMjYzOGIzYTExOWY2'
        ),
    );

    $response = $http->post('https://onesignal.com/api/v1/notifications', $fields, $request);
    
  }
}