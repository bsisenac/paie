<?php
App::uses('AppController', 'Controller');
App::uses( "HttpSocket", "Network/Http" );

/**
 * Notificacoes Controller
 *
 * @property Notificacao $Notificacao
 * @property PaginatorComponent $Paginator
 */
class NotificacoesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'PushNotificacao');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Notificacao->recursive = 0;
		$this->set('notificacoes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Notificacao->exists($id)) {
			throw new NotFoundException(__('Invalid notificacao'));
		}
		$options = array('conditions' => array('Notificacao.' . $this->Notificacao->primaryKey => $id));
		$this->set('notificacao', $this->Notificacao->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Notificacao->create();
			if ($this->Notificacao->save($this->request->data)) {
				$this->Session->setFlash(__('The notificacao has been saved.'));
				$this->PushNotificacao->enviarNotificacao( "Nova mensagem", $this->request->data["Notificacao"]["texto"] );

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The notificacao could not be saved. Please, try again.'));
			}
		}
		$users = $this->Notificacao->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Notificacao->exists($id)) {
			throw new NotFoundException(__('Invalid notificacao'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Notificacao->save($this->request->data)) {
				$this->Session->setFlash(__('The notificacao has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The notificacao could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Notificacao.' . $this->Notificacao->primaryKey => $id));
			$this->request->data = $this->Notificacao->find('first', $options);
		}
		$users = $this->Notificacao->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Notificacao->id = $id;
		if (!$this->Notificacao->exists()) {
			throw new NotFoundException(__('Invalid notificacao'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Notificacao->delete()) {
			$this->Session->setFlash(__('The notificacao has been deleted.'));
		} else {
			$this->Session->setFlash(__('The notificacao could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
