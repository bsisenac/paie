<?php

$_pluralIrregular = array(
	'user' => 'users',
	'escola' => 'escolas',
	'turma' => 'turmas',
	'disciplina' => 'disciplinas',
	'turmas_disciplina' => 'turmas_disciplinas',
	'notificacao' => 'notificacoes',
	'transportes_user' => 'transportes_users'
);

$_uninflected = array('regiao','user','atlas', 'lapis', 'onibus', 'pires', 'virus', '.*x', 'status', 'busca');

Inflector::rules('plural', array(
'rules' => array(
'/^(.*)ao$/i' => '\1oes',
'/^(.*)(r|s|z)$/i' => '\1\2es',
'/^(.*)(a|e|o|u)l$/i' => '\1\2is',
'/^(.*)il$/i' => '\1is',
'/^(.*)(m|n)$/i' => '\1ns',
'/^(.*)$/i' => '\1s'
),
'uninflected' => $_uninflected,
'irregular' => $_pluralIrregular
), true);

?>
