<div class="usersTurmas view">
	<h2><?php echo __('Users Turma'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><?php echo __('Id'); ?></td>
			<td><?php echo h($usersTurma['UsersTurma']['id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('User'); ?></td>
			<td>
				<?php echo $this->Html->link($usersTurma['User']['fullname'], array('controller' => 'users', 'action' => 'view', $usersTurma['User']['id'])); ?>
			</td>
		<	r>

		<tr>
			<td><?php echo __('Turma'); ?></td>
			<td>
				<?php echo $this->Html->link($usersTurma['Turma']['id'], array('controller' => 'turmas', 'action' => 'view', $usersTurma['Turma']['id'])); ?>
			</td>
		<	r>
	</table>
</div>
<div class="actions">
	<div class="row">
		<h3><?php echo __('Opções'); ?></h3>

				<?php echo $this->Html->link(__('Editar Users Turma'), array('action' => 'edit', $usersTurma['UsersTurma']['id']), array('class' => 'btn btn-primary')); ?>
		<?php echo $this->Form->postLink(__('Deletar Users Turma'), array('action' => 'delete', $usersTurma['UsersTurma']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $usersTurma['UsersTurma']['id'])); ?>
		<?php echo $this->Html->link(__('Ver Users Turmas'), array('action' => 'index'), array('class' => 'btn btn-warning')); ?>
		<?php echo $this->Html->link(__('Adicionar Users Turma'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
	</div>
	<div class="row">
		<h3><?php echo __('Itens Relacionados'); ?></h3>
				<?php echo $this->Html->link(__('Ver Users'), array('controller' => 'users', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Ver Turmas'), array('controller' => 'turmas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar Turma'), array('controller' => 'turmas', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
	</div>
</div>
