<div class="usersTurmas index">
	<h2><?php echo __('Users Turmas'); ?></h2>
	<div class="row">
		<div class="actions col-xs-12">
			<?php echo $this->Html->link(__('Adicionar Users Turma'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">Descrição da Tabela</div>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed">
				<thead>
					<tr>
												<th><?php echo $this->Paginator->sort('id'); ?></th>
												<th><?php echo $this->Paginator->sort('user_id'); ?></th>
												<th><?php echo $this->Paginator->sort('turma_id'); ?></th>
												<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usersTurmas as $usersTurma): ?>
	<tr>
		<td><?php echo h($usersTurma['UsersTurma']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($usersTurma['User']['fullname'], array('controller' => 'users', 'action' => 'view', $usersTurma['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($usersTurma['Turma']['id'], array('controller' => 'turmas', 'action' => 'view', $usersTurma['Turma']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $usersTurma['UsersTurma']['id']), array('class' => 'btn btn-info btn-block')); ?>
		</td>
	</tr>
<?php endforeach; ?>
				</tbody>
			</table>
			<div class="panel-footer">
				<div class="paging">
					<ul class="pagination text-center">						
						<li><?php 		echo $this->Paginator->prev(__('Anterior'), array(), null, array('class' => 'prev disabled'));	?></li>
						<li><?php 		echo $this->Paginator->numbers(array('separator' => ''));	?></li>
						<li><?php 		echo $this->Paginator->next(__('Próxima'), array(), null, array('class' => 'next disabled'));	?> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="actions col-xs-12">
			<?php echo $this->Html->link(__('Adicionar Users Turma'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>		</div>
	</div>
</div>
