<div class="usersTurmas form">
	<legend><?php echo __('Edit Users Turma'); ?></legend>
<?php echo $this->Form->create('UsersTurma', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('user_id', array('class' => 'form-control'));
		echo $this->Form->input('turma_id', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Opções'); ?></h3>

<div class="row">
		<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $this->Form->value('UsersTurma.id')), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $this->Form->value('UsersTurma.id'))); ?>		<?php echo $this->Html->link(__('Ver Users Turmas'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>		</div>
<div class='row'>		<?php echo $this->Html->link(__('Ver Users'), array('controller' => 'users', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adicionar User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-success')); ?>
</div><div class='row'>		<?php echo $this->Html->link(__('Ver Turmas'), array('controller' => 'turmas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adicionar Turma'), array('controller' => 'turmas', 'action' => 'add'), array('class' => 'btn btn-success')); ?>
</div></div>
