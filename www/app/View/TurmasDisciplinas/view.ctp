<div class="turmasDisciplinas view">
	<h2><?php echo __('Turmas Disciplina'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><?php echo __('Id'); ?></td>
			<td><?php echo h($turmasDisciplina['TurmasDisciplina']['id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('Disciplina'); ?></td>
			<td>
				<?php echo $this->Html->link($turmasDisciplina['Disciplina']['id'], array('controller' => 'disciplinas', 'action' => 'view', $turmasDisciplina['Disciplina']['id'])); ?>
			</td>
		<	r>

		<tr>
			<td><?php echo __('Turma'); ?></td>
			<td>
				<?php echo $this->Html->link($turmasDisciplina['Turma']['id'], array('controller' => 'turmas', 'action' => 'view', $turmasDisciplina['Turma']['id'])); ?>
			</td>
		<	r>
	</table>
</div>
<div class="actions">
	<div class="row">
		<h3><?php echo __('Opções'); ?></h3>

				<?php echo $this->Html->link(__('Editar Turmas Disciplina'), array('action' => 'edit', $turmasDisciplina['TurmasDisciplina']['id']), array('class' => 'btn btn-primary')); ?>
		<?php echo $this->Form->postLink(__('Deletar Turmas Disciplina'), array('action' => 'delete', $turmasDisciplina['TurmasDisciplina']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $turmasDisciplina['TurmasDisciplina']['id'])); ?>
		<?php echo $this->Html->link(__('Ver Turmas Disciplinas'), array('action' => 'index'), array('class' => 'btn btn-warning')); ?>
		<?php echo $this->Html->link(__('Adicionar Turmas Disciplina'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
	</div>
	<div class="row">
		<h3><?php echo __('Itens Relacionados'); ?></h3>
				<?php echo $this->Html->link(__('Ver Disciplinas'), array('controller' => 'disciplinas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar Disciplina'), array('controller' => 'disciplinas', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Ver Turmas'), array('controller' => 'turmas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar Turma'), array('controller' => 'turmas', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
	</div>
</div>
