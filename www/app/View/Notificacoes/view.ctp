<div class="notificacoes view row">
	<h2><?php echo __('Notificação'); ?> # <?php echo h($notificacao['Notificacao']['id']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($notificacao['Notificacao']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Texto'); ?></strong></td>
			<td><?php echo h($notificacao['Notificacao']['texto']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('User'); ?></strong></td>
			<td>
				<?php echo $this->Html->link($notificacao['User']['fullname'], array('controller' => 'users', 'action' => 'view', $notificacao['User']['id'])); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar Notificação'), array('action' => 'edit', $notificacao['Notificacao']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar Notificação'), array('action' => 'delete', $notificacao['Notificacao']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $notificacao['Notificacao']['id'])); ?>
			</td>
		</tr>
	</table>
</div>
