<div class="escolas form row">
    <h3 class="page-header">Editar Notificação</h3>
<?php echo $this->Form->create('Notificacao', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('texto', array('class' => 'form-control'));
		echo $this->Form->input('user_id', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
