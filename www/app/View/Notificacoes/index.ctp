            <div class="row">
            	<div class="col-lg-12">
            		<h2 class="page-header">Notificações <a class="btn btn-sm btn-primary" href="/notificacoes/add">Cadastrar notificação</a></h2>
            	</div>
            	<!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
            		<div class="panel panel-default">
            			<div class="panel-heading">
            				Lista de Notificações do Sistema
            			</div>
            			<!-- /.panel-heading -->
            			<div class="panel-body">
            				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-notificacoes">
            					<thead>
            						<tr>
            							<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('texto'); ?></th>
										<th><?php echo $this->Paginator->sort('user_id'); ?></th>
            							<th class="actions"><?php echo __('ver'); ?></th>

            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach ($notificacoes as $notificacao): ?>
										<tr>
											<td><?php echo h($notificacao['Notificacao']['id']); ?>&nbsp;</td>
											<td><?php echo h($notificacao['Notificacao']['texto']); ?>&nbsp;</td>
											<td>
												<?php echo $this->Html->link($notificacao['User']['fullname'], array('controller' => 'users', 'action' => 'view', $notificacao['User']['id'])); ?>
											</td>
											<td class="actions">
												<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $notificacao['Notificacao']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
            				</tbody>
            			</table>
            			<!-- /.table-responsive -->
            		</div>
            		<!-- /.panel-body -->
            	</div>
            	<!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
        	$('#dataTables-notificacoes').DataTable({
        		responsive: true
        	});
        });
        </script>