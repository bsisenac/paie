<div class="widgets index">
	<h2><?php echo __('Widgets'); ?></h2>
	<div class="row">
		<div class="actions col-xs-12">
			<?php echo $this->Html->link(__('Adicionar Widget'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">Descrição da Tabela</div>
		<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed">
				<thead>
					<tr>
												<th><?php echo $this->Paginator->sort('id'); ?></th>
												<th><?php echo $this->Paginator->sort('name'); ?></th>
												<th><?php echo $this->Paginator->sort('part_no'); ?></th>
												<th><?php echo $this->Paginator->sort('quantity'); ?></th>
												<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($widgets as $widget): ?>
	<tr>
		<td><?php echo h($widget['Widget']['id']); ?>&nbsp;</td>
		<td><?php echo h($widget['Widget']['name']); ?>&nbsp;</td>
		<td><?php echo h($widget['Widget']['part_no']); ?>&nbsp;</td>
		<td><?php echo h($widget['Widget']['quantity']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $widget['Widget']['id']), array('class' => 'btn btn-info btn-block')); ?>
		</td>
	</tr>
<?php endforeach; ?>
				</tbody>
			</table>
			<div class="panel-footer">
				<div class="paging">
					<ul class="pagination text-center">						
						<li><?php 		echo $this->Paginator->prev(__('Anterior'), array(), null, array('class' => 'prev disabled'));	?></li>
						<li><?php 		echo $this->Paginator->numbers(array('separator' => ''));	?></li>
						<li><?php 		echo $this->Paginator->next(__('Próxima'), array(), null, array('class' => 'next disabled'));	?> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="actions col-xs-12">
			<?php echo $this->Html->link(__('Adicionar Widget'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>		</div>
	</div>
</div>
