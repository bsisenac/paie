<div class="widgets view">
	<h2><?php echo __('Widget'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><?php echo __('Id'); ?></td>
			<td><?php echo h($widget['Widget']['id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('Name'); ?></td>
			<td><?php echo h($widget['Widget']['name']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('Part No'); ?></td>
			<td><?php echo h($widget['Widget']['part_no']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('Quantity'); ?></td>
			<td><?php echo h($widget['Widget']['quantity']); ?></td>
		</tr>
	</table>
</div>
<div class="actions">
	<div class="row">
		<h3><?php echo __('Opções'); ?></h3>

				<?php echo $this->Html->link(__('Editar Widget'), array('action' => 'edit', $widget['Widget']['id']), array('class' => 'btn btn-primary')); ?>
		<?php echo $this->Form->postLink(__('Deletar Widget'), array('action' => 'delete', $widget['Widget']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $widget['Widget']['id'])); ?>
		<?php echo $this->Html->link(__('Ver Widgets'), array('action' => 'index'), array('class' => 'btn btn-warning')); ?>
		<?php echo $this->Html->link(__('Adicionar Widget'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
	</div>
	<div class="row">
		<h3><?php echo __('Itens Relacionados'); ?></h3>
			</div>
</div>
