<div class="widgets form">
	<legend><?php echo __('Add Widget'); ?></legend>
<?php echo $this->Form->create('Widget', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('name', array('class' => 'form-control'));
		echo $this->Form->input('part_no', array('class' => 'form-control'));
		echo $this->Form->input('quantity', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Opções'); ?></h3>

<div class="row">
		<?php echo $this->Html->link(__('Ver Widgets'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>		</div>
</div>
