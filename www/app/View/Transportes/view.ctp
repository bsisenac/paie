<div class="transportes view row">
	<h2><?php echo __('Transporte'); ?> # <?php echo h($transporte['Transporte']['id']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($transporte['Transporte']['id']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Marca Veiculo'); ?></td>
			<td><?php echo h($transporte['Transporte']['marca_veiculo']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Placa Veiculo'); ?></td>
			<td><?php echo h($transporte['Transporte']['placa_veiculo']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Ano Fabricacao Veiculo'); ?></td>
			<td><?php echo h($transporte['Transporte']['ano_fabricacao_veiculo']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Documentacao Veiculo'); ?></td>
			<td><?php echo h($transporte['Transporte']['documentacao_veiculo']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Documentacao Pendente'); ?></td>
			<td><?php echo h($transporte['Transporte']['documentacao_pendente']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Total Quantidade Vagas'); ?></td>
			<td><?php echo h($transporte['Transporte']['total_quantidade_vagas']); ?></strong></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Quantidade Vagas Atual'); ?></td>
			<td><?php echo h($transporte['Transporte']['quantidade_vagas_atual']); ?></strong></td>
		</tr>
		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar Transporte'), array('action' => 'edit', $transporte['Transporte']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar Transporte'), array('action' => 'delete', $transporte['Transporte']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $transporte['Transporte']['id'])); ?>
			</td>
		</table>
	</div>

	<div class="row">
		<div class="related">
			<?php if (!empty($transporte['User'])): ?>
			<div class="panel panel-default">
				<h3><?php echo __('Pessoas relacionadas'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped table-hover table-condensed">
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Grupo'); ?></th>
							<th><?php echo __('Escola'); ?></th>
							<th><?php echo __('Nome'); ?></th>
							<th><?php echo __('Sobrenome'); ?></th>
							<th><?php echo __('Rg'); ?></th>
							<th><?php echo __('Cpf'); ?></th>
							<th><?php echo __('Telefone'); ?></th>
							<th><?php echo __('Endereco'); ?></th>
							<th><?php echo __('Estuda Periodo'); ?></th>
							<th><?php echo __('Hora Busca'); ?></th>
							<th><?php echo __('Hora Entrega'); ?></th>
							<th class="actions"><?php echo __('Opções'); ?></th>
						</tr>
						<?php foreach ($transporte['User'] as $user): ?>
						<tr>
							<td><?php echo $user['id']; ?></td>
							<td><?php echo $user['Grupo']['name']; ?></td>
							<td><?php echo $user['Escola']['nome']; ?></td>
							<td><?php echo $user['nome'] . ' ' . $user['sobrenome']; ?></td>
							<td><?php echo $user['rg']; ?></td>
							<td><?php echo $user['cpf']; ?></td>
							<td><?php echo $user['data_nascimento']; ?></td>
							<td><?php echo $user['telefone']; ?></td>
							<td><?php echo $user['logradouro_endereco'] . ', ' . $user['numero_endereco'] . ' | ' . $user['cep']; ?></td>
							<td><?php echo $user['estuda_periodo']; ?></td>
							<td><?php echo $user['hora_busca']; ?></td>
							<td><?php echo $user['hora_entrega']; ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-sm btn-info')); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	<?php endif; ?>
	</div>
</div>
