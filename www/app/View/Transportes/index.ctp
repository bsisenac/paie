            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Transportes <a class="btn btn-sm btn-primary" href="/transportes/add">Cadastrar transporte</a></h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista de Transportes do Sistema
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-transportes">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('marca_veiculo'); ?></th>
										<th><?php echo $this->Paginator->sort('placa_veiculo'); ?></th>
										<th><?php echo $this->Paginator->sort('ano_fabricacao_veiculo'); ?></th>
										<th><?php echo $this->Paginator->sort('documentacao_veiculo'); ?></th>
										<th><?php echo $this->Paginator->sort('documentacao_pendente'); ?></th>
										<th><?php echo $this->Paginator->sort('total_quantidade_vagas'); ?></th>
										<th><?php echo $this->Paginator->sort('quantidade_vagas_atual'); ?></th>
                                        <th class="actions"><?php echo __('ver'); ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php foreach ($transportes as $transporte): ?>
										<tr>
											<td><?php echo h($transporte['Transporte']['id']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['marca_veiculo']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['placa_veiculo']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['ano_fabricacao_veiculo']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['documentacao_veiculo']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['documentacao_pendente']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['total_quantidade_vagas']); ?>&nbsp;</td>
											<td><?php echo h($transporte['Transporte']['quantidade_vagas_atual']); ?>&nbsp;</td>
											<td class="actions">
                                                <?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $transporte['Transporte']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
                                            </td>
										</tr>
									<?php endforeach; ?>                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
            $('#dataTables-transportes').DataTable({
                responsive: true
            });
        });
        </script>