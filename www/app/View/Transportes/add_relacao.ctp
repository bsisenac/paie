<div class="escolas form row">
    <h3 class="page-header">Cadastrar Relação Transporte</h3>
<?php echo $this->Form->create('TransportesUser', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('user_id', array('class' => 'form-control'));
		echo $this->Form->input('transporte_id', array('class' => 'form-control'));
		echo $this->Form->input('relacao_transporte', array('class' => 'form-control', 'options' => array('motorista' => 'Motorista', 'aluno' => 'Aluno')));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
