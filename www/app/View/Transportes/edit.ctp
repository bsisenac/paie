<div class="users form row">
	<h3 class="page-header">Editar Transporte</h3>
	<?php echo $this->Form->create('Transporte', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
		<?php
    		echo $this->Form->input('id', array('class' => 'form-control'));
			echo $this->Form->input('marca_veiculo', array('class' => 'form-control'));
			echo $this->Form->input('placa_veiculo', array('class' => 'form-control'));
			echo $this->Form->input('ano_fabricacao_veiculo', array('class' => 'form-control'));
			echo $this->Form->input('documentacao_veiculo', array('class' => false));
			echo $this->Form->input('documentacao_pendente', array('class' => 'form-control'));
			echo $this->Form->input('total_quantidade_vagas', array('class' => 'form-control'));
			echo $this->Form->input('quantidade_vagas_atual', array('class' => 'form-control'));
		?>
	</fieldset>
	<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
	<?php echo $this->Form->end(); ?>
</div>