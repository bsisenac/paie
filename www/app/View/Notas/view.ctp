<div class="notas view row">
	<h2><?php echo __('Nota'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($nota['Nota']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Nota'); ?></strong></td>
			<td><?php echo h($nota['Nota']['nota']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Disciplina'); ?></strong></td>
			<td>
				<?php echo $this->Html->link($nota['Disciplina']['nome'], array('controller' => 'disciplinas', 'action' => 'view', $nota['Disciplina']['id'])); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('User'); ?></strong></td>
			<td>
				<?php echo $this->Html->link($nota['User']['fullname'], array('controller' => 'users', 'action' => 'view', $nota['User']['id'])); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Created'); ?></strong></td>
			<td><?php echo h($nota['Nota']['created']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Modified'); ?></strong></td>
			<td><?php echo h($nota['Nota']['modified']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar Nota'), array('action' => 'edit', $nota['Nota']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar Nota'), array('action' => 'delete', $nota['Nota']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $nota['Nota']['id'])); ?>
			</td>
		</tr>
	</table>
</div>

