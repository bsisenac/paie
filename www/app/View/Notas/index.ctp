            <div class="row">
            	<div class="col-lg-12">
            		<h2 class="page-header">Notas <a class="btn btn-sm btn-primary" href="/notas/add">Cadastrar nota</a></h2>
            	</div>
            	<!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
            		<div class="panel panel-default">
            			<div class="panel-heading">
            				Lista de Notas de Alunos
            			</div>
            			<!-- /.panel-heading -->
            			<div class="panel-body">
            				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-notas">
            					<thead>
            						<tr>
            							<th><?php echo $this->Paginator->sort('id'); ?></th>
            							<th><?php echo $this->Paginator->sort('nota'); ?></th>
            							<th><?php echo $this->Paginator->sort('disciplina_id'); ?></th>
            							<th><?php echo $this->Paginator->sort('user_id'); ?></th>
            							<th><?php echo $this->Paginator->sort('created'); ?></th>
            							<th><?php echo $this->Paginator->sort('modified'); ?></th>
            							<th class="actions"><?php echo __('ver'); ?></th>

            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach ($notas as $nota): ?>
            						<tr>
            							<td><?php echo h($nota['Nota']['id']); ?>&nbsp;</td>
            							<td><?php echo h($nota['Nota']['nota']); ?>&nbsp;</td>
            							<td>
            								<?php echo $this->Html->link($nota['Disciplina']['nome'], array('controller' => 'disciplinas', 'action' => 'view', $nota['Disciplina']['id'])); ?>
            							</td>
            							<td>
            								<?php echo $this->Html->link($nota['User']['fullname'], array('controller' => 'users', 'action' => 'view', $nota['User']['id'])); ?>
            							</td>
            							<td><?php echo h($nota['Nota']['created']); ?>&nbsp;</td>
            							<td><?php echo h($nota['Nota']['modified']); ?>&nbsp;</td>
            							<td class="actions">
            								<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $nota['Nota']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
            							</td>
            						</tr>
            					<?php endforeach; ?>
            				</tbody>

            			</tbody>
            		</table>
            		<!-- /.table-responsive -->
            	</div>
            	<!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
    	$('#dataTables-notas').DataTable({
    		responsive: true
    	});
    });
    </script>