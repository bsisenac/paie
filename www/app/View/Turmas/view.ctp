<div class="turmas view row">
	<h2><?php echo __('Turma'); ?> # <?php echo h($turma['Turma']['nome']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($turma['Turma']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Nome'); ?></strong></td>
			<td><?php echo h($turma['Turma']['nome']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Codigo'); ?></strong></td>
			<td><?php echo h($turma['Turma']['codigo']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Periodo'); ?></strong></td>
			<td><?php echo h($turma['Turma']['periodo']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Escola'); ?></strong></td>
			<td>
				<?php echo $this->Html->link($turma['Escola']['nome'], array('controller' => 'escolas', 'action' => 'view', $turma['Escola']['id'])); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar turma'), array('action' => 'edit', $turma['Turma']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar turma'), array('action' => 'delete', $turma['Turma']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $turma['Turma']['id'])); ?>
			</td>
		</tr>
	</table>
</div>

<div class="row">
	<div class="related">

		<?php if (!empty($turma['Disciplina'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Disciplinas'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Nome'); ?></th>
						<th><?php echo __('Codigo'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($turma['Disciplina'] as $disciplina): ?>
					<tr>
						<td><?php echo $disciplina['id']; ?></td>
						<td><?php echo $disciplina['nome']; ?></td>
						<td><?php echo $disciplina['codigo']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Ver'), array('controller' => 'disciplinas', 'action' => 'view', $disciplina['id']), array('class' => 'btn btn-sm btn-info')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
	<div class="actions">
		<?php echo $this->Html->link(__('Adicionar Disciplina'), array('controller' => 'disciplinas', 'action' => 'add'), array('class' => 'btn btn-success')); ?>		</div>
	</div>
<?php endif; ?>


</div>
<div class="row">
	<div class="related">

		<?php if (!empty($turma['User'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Users'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Grupo Id'); ?></th>
						<th><?php echo __('Username'); ?></th>
						<th><?php echo __('Nome'); ?></th>
						<th><?php echo __('Sobrenome'); ?></th>
						<th><?php echo __('Password'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($turma['User'] as $user): ?>
					<tr>
						<td><?php echo $user['id']; ?></td>
						<td><?php echo $user['grupo_id']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['nome']; ?></td>
						<td><?php echo $user['sobrenome']; ?></td>
						<td><?php echo $user['password']; ?></td>
						<td><?php echo $user['created']; ?></td>
						<td><?php echo $user['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-sm btn-info')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
	<div class="actions">
		<?php echo $this->Html->link(__('Adicionar User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-success')); ?>	
	</div>
<?php endif; ?>


</div>
</div>
