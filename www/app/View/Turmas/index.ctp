            <div class="row">
            	<div class="col-lg-12">
            		<h2 class="page-header">Turmas <a class="btn btn-sm btn-primary" href="/turmas/add">Cadastrar turma</a></h2>
            	</div>
            	<!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
            		<div class="panel panel-default">
            			<div class="panel-heading">
            				Lista de Turmas do Sistema
            			</div>
            			<!-- /.panel-heading -->
            			<div class="panel-body">
            				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-turmas">
            					<thead>
            						<tr>
            							<th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('nome'); ?></th>
										<th><?php echo $this->Paginator->sort('codigo'); ?></th>
										<th><?php echo $this->Paginator->sort('periodo'); ?></th>
										<th><?php echo $this->Paginator->sort('escola'); ?></th>
            							<th class="actions"><?php echo __('ver'); ?></th>

            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach ($turmas as $turma): ?>
										<tr>
											<td><?php echo h($turma['Turma']['id']); ?>&nbsp;</td>
											<td><?php echo h($turma['Turma']['nome']); ?>&nbsp;</td>
											<td><?php echo h($turma['Turma']['codigo']); ?>&nbsp;</td>
											<td><?php echo h($turma['Turma']['periodo']); ?>&nbsp;</td>
											<td>
												<?php echo $this->Html->link($turma['Escola']['nome'], array('controller' => 'escolas', 'action' => 'view', $turma['Escola']['id'])); ?>
											</td>
											<td class="actions">
												<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $turma['Turma']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
            				</tbody>
            			</table>
            			<!-- /.table-responsive -->
            		</div>
            		<!-- /.panel-body -->
            	</div>
            	<!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
        	$('#dataTables-turmas').DataTable({
        		responsive: true
        	});
        });
        </script>