<div class="escolas form row">
    <h3 class="page-header">Editar Turma</h3>
<?php echo $this->Form->create('Turma', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('nome', array('class' => 'form-control'));
		echo $this->Form->input('codigo', array('class' => 'form-control'));
		echo $this->Form->input('periodo', array('class' => 'form-control'));
		echo $this->Form->input('escola_id', array('class' => 'form-control'));
		echo $this->Form->input('Disciplina');
		echo $this->Form->input('User');
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>