<div class="usersDisciplinas form">
	<legend><?php echo __('Add Users Disciplina'); ?></legend>
<?php echo $this->Form->create('UsersDisciplina', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('user_id', array('class' => 'form-control'));
		echo $this->Form->input('disciplina_id', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Opções'); ?></h3>

<div class="row">
		<?php echo $this->Html->link(__('Ver Users Disciplinas'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>		</div>
<div class='row'>		<?php echo $this->Html->link(__('Ver Users'), array('controller' => 'users', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adicionar User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-success')); ?>
</div><div class='row'>		<?php echo $this->Html->link(__('Ver Disciplinas'), array('controller' => 'disciplinas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adicionar Disciplina'), array('controller' => 'disciplinas', 'action' => 'add'), array('class' => 'btn btn-success')); ?>
</div></div>
