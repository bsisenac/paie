<div class="usersDisciplinas view">
	<h2><?php echo __('Users Disciplina'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><?php echo __('Id'); ?></td>
			<td><?php echo h($usersDisciplina['UsersDisciplina']['id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('User'); ?></td>
			<td>
				<?php echo $this->Html->link($usersDisciplina['User']['fullname'], array('controller' => 'users', 'action' => 'view', $usersDisciplina['User']['id'])); ?>
			</td>
		<	r>

		<tr>
			<td><?php echo __('Disciplina'); ?></td>
			<td>
				<?php echo $this->Html->link($usersDisciplina['Disciplina']['id'], array('controller' => 'disciplinas', 'action' => 'view', $usersDisciplina['Disciplina']['id'])); ?>
			</td>
		<	r>
	</table>
</div>
<div class="actions">
	<div class="row">
		<h3><?php echo __('Opções'); ?></h3>

				<?php echo $this->Html->link(__('Editar Users Disciplina'), array('action' => 'edit', $usersDisciplina['UsersDisciplina']['id']), array('class' => 'btn btn-primary')); ?>
		<?php echo $this->Form->postLink(__('Deletar Users Disciplina'), array('action' => 'delete', $usersDisciplina['UsersDisciplina']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $usersDisciplina['UsersDisciplina']['id'])); ?>
		<?php echo $this->Html->link(__('Ver Users Disciplinas'), array('action' => 'index'), array('class' => 'btn btn-warning')); ?>
		<?php echo $this->Html->link(__('Adicionar Users Disciplina'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
	</div>
	<div class="row">
		<h3><?php echo __('Itens Relacionados'); ?></h3>
				<?php echo $this->Html->link(__('Ver Users'), array('controller' => 'users', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Ver Disciplinas'), array('controller' => 'disciplinas', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Adiconar Disciplina'), array('controller' => 'disciplinas', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
	</div>
</div>
