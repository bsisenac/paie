<div class="users view row">
	<h2><?php echo __('User'); ?> # <?php echo h($user['User']['nome']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($user['User']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Grupo'); ?></strong></td>
			<td><?php echo $this->Html->link($user['Grupo']['name'], array('controller' => 'grupos', 'action' => 'view', $user['Grupo']['id'])); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Username'); ?></strong></td>
			<td><?php echo h($user['User']['username']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Nome'); ?></strong></td>
			<td><?php echo h($user['User']['nome']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Sobrenome'); ?></strong></td>
			<td>
				<?php echo h($user['User']['sobrenome']); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Password'); ?></strong></td>
			<td>
				<?php echo h($user['User']['password']); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Created'); ?></strong></td>
			<td>
				<?php echo h($user['User']['created']); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Modified'); ?></strong></td>
			<td>
				<?php echo h($user['User']['modified']); ?>
			</td>
		</tr>

		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar user'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar user'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
			</td>
		</tr>
	</table>
</div>

</div>
<div class="row">
	<div class="related">
		
		<?php if (!empty($user['Comentario'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Comentarios'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Comentario Desc'); ?></th>
						<th><?php echo __('Documento Id'); ?></th>
						<th><?php echo __('Reacao Id'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Comentario Id'); ?></th>
						<th><?php echo __('Numero Bloco'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Motivo Status'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($user['Comentario'] as $comentario): ?>
					<tr>
						<td><?php echo $comentario['id']; ?></td>
						<td><?php echo $comentario['comentario_desc']; ?></td>
						<td><?php echo $comentario['documento_id']; ?></td>
						<td><?php echo $comentario['reacao_id']; ?></td>
						<td><?php echo $comentario['user_id']; ?></td>
						<td><?php echo $comentario['comentario_id']; ?></td>
						<td><?php echo $comentario['numero_bloco']; ?></td>
						<td><?php echo $comentario['status']; ?></td>
						<td><?php echo $comentario['motivo_status']; ?></td>
						<td><?php echo $comentario['created']; ?></td>
						<td><?php echo $comentario['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Visualizar'), array('controller' => 'comentarios', 'action' => 'view', $comentario['id']), array('class' => 'btn btn-default')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
<div class="row">
	<div class="related">
		
		<?php if (!empty($user['Documento'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Documentos'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Nome'); ?></th>
						<th><?php echo __('Descricao'); ?></th>
						<th><?php echo __('Conteudo'); ?></th>
						<th><?php echo __('Documento Id'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Motivo Status'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th><?php echo __('Data Encerramento'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($user['Documento'] as $documento): ?>
					<tr>
						<td><?php echo $documento['id']; ?></td>
						<td><?php echo $documento['nome']; ?></td>
						<td><?php echo $documento['descricao']; ?></td>
						<td><?php echo $documento['conteudo']; ?></td>
						<td><?php echo $documento['documento_id']; ?></td>
						<td><?php echo $documento['user_id']; ?></td>
						<td><?php echo $documento['status']; ?></td>
						<td><?php echo $documento['motivo_status']; ?></td>
						<td><?php echo $documento['created']; ?></td>
						<td><?php echo $documento['modified']; ?></td>
						<td><?php echo $documento['data_encerramento']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Visualizar'), array('controller' => 'documentos', 'action' => 'view', $documento['id']), array('class' => 'btn btn-default')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
<div class="row">
	<div class="related">
		
		<?php if (!empty($user['Lixeira'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Lixeira'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Model Id'); ?></th>
						<th><?php echo __('Model Desc'); ?></th>
						<th><?php echo __('Estrutura'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($user['Lixeira'] as $lixeira): ?>
					<tr>
						<td><?php echo $lixeira['id']; ?></td>
						<td><?php echo $lixeira['model_id']; ?></td>
						<td><?php echo $lixeira['model_desc']; ?></td>
						<td><?php echo $lixeira['estrutura']; ?></td>
						<td><?php echo $lixeira['user_id']; ?></td>
						<td><?php echo $lixeira['created']; ?></td>
						<td><?php echo $lixeira['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Visualizar'), array('controller' => 'lixeira', 'action' => 'view', $lixeira['id']), array('class' => 'btn btn-default')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
<div class="row">
	<div class="related">
		
		<?php if (!empty($user['Log'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Logs'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Model Id'); ?></th>
						<th><?php echo __('Model Desc'); ?></th>
						<th><?php echo __('Descricao'); ?></th>
						<th><?php echo __('Acao'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($user['Log'] as $log): ?>
					<tr>
						<td><?php echo $log['id']; ?></td>
						<td><?php echo $log['model_id']; ?></td>
						<td><?php echo $log['model_desc']; ?></td>
						<td><?php echo $log['descricao']; ?></td>
						<td><?php echo $log['acao']; ?></td>
						<td><?php echo $log['user_id']; ?></td>
						<td><?php echo $log['created']; ?></td>
						<td><?php echo $log['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Visualizar'), array('controller' => 'logs', 'action' => 'view', $log['id']), array('class' => 'btn btn-default')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>

</div>
</div>
<div class="row">
	<div class="related">
		
		<?php if (!empty($user['Reacao'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Related Reacoes'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Descricao'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Status'); ?></th>
						<th><?php echo __('Motivo Status'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($user['Reacao'] as $reacao): ?>
					<tr>
						<td><?php echo $reacao['id']; ?></td>
						<td><?php echo $reacao['descricao']; ?></td>
						<td><?php echo $reacao['user_id']; ?></td>
						<td><?php echo $reacao['status']; ?></td>
						<td><?php echo $reacao['motivo_status']; ?></td>
						<td><?php echo $reacao['created']; ?></td>
						<td><?php echo $reacao['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Visualizar'), array('controller' => 'reacoes', 'action' => 'view', $reacao['id']), array('class' => 'btn btn-default')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
