<div class="container">
   <div class="row">
    <div id="login" class="col-md-6 col-md-offset-3"> 

        <div class="panel panel-default" >

            <div class="panel-heading panel-form-customizado">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="panel-title pull-left"><i class="fa fa-lock"> </i> Trocar a senha</h1><br/>
                    </div>
                    <div class="col-md-12">
                        <small class="text-danger">É necessário informar a senha duas vezes corretamente para confirmação da troca.</small>
                    </div>
                </div>
            </div>     

            <div class="panel-body" >

               <?php 
               $message = $this->Session->flash(); 
               if( $message ) 
                echo preg_replace( '/(<.*class="*)(message)/', "$1alert alert-warning", $message );
            ?>

            <?php echo $this->Form->create( 'User', array(
                'url' => array(
                    'controller' => 'users',
                    'action' => 'alterar_senha',
                    ),
                'class' => 'form-horizontal',
                'role' => 'form'
                
                )); ?>

                <div class="input-group senha">
                    <label class="input-group-addon" title="Senha de acesso"><i class="fa fa-unlock-alt"></i></label>
                    <?php echo $this->Form->input('User.password', array( "class" =>"form-control input-lg", 'placeholder' => "Senha de acesso", "div" => false, "label" => false, 'title' => "Senha de acesso" )); ?>
                </div>
                <div class="input-group senha">
                    <label class="input-group-addon" title="Senha de acesso"><i class="fa fa-unlock-alt"></i></label>
                    <?php echo $this->Form->input('User.password_confirmacao', array( "class" =>"form-control input-lg", 'placeholder' => "Senha de acesso", "div" => false, "label" => false, 'title' => "Confirmação da senha", 'type' => "password" )); ?>
                </div>
                <div class="form-group">

                    <div class="col-md-12">
                        <div class="controls">

                            <input type="hidden" name="model" value="login">
                            <button type="submit" id="btn-login" class="btn btn-danger btn-md">Confirmar</button>

                        </div>
                    </div>
                </div>    

                <?php echo $this->Form->end(); ?>     
            </div>
            <div class="panel-footer" >
                <div class="row resetar-senha">
                    <div class="col-md-12">
                    <small class='text-danger'>Sua senha deve conter no mínimo 8 caracteres.</small>
                    <ul>
                        <li>
                            <small class='text-danger'>1 ou mais números.</small>
                        </li>
                        <li>
                            <small class='text-danger'>1 ou mais letras maiúsculas.</small>
                        </li>
                        <li>
                            <small class='text-danger'>1 ou mais caracteres especiais.</small>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>                                           
        </div>  
    </div>
</div>
</div>