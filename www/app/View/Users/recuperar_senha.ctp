<div class="container">
   <div class="row">
    <div id="login" class="col-md-6 col-md-offset-3"> 

        <div class="panel panel-default" >

            <div class="panel-heading panel-form-customizado">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="panel-title pull-left"><i class="fa fa-unlock-alt"> </i> Recuperar senha</h1><br/>
                    </div>
                    <div class="col-md-12">
                        <small class="text-danger">Informe seu e-mail para recuperação da senha.</small>
                    </div>
                </div>
            </div>     

            <div class="panel-body" >

               <?php 
               $message = $this->Session->flash(); 
               if( $message ) 
                echo preg_replace( '/(<.*class="*)(message)/', "$1alert alert-warning", $message );
            ?>

            <?php echo $this->Form->create( 'User', array(
                'url' => array(
                    'controller' => 'users',
                    'action' => 'recuperar_senha',
                    ),
                'class' => 'form-horizontal',
                'role' => 'form'
                
                )); ?>

                <div class="input-group usuario">
                    <label class="input-group-addon" title="Usuário"><i class="fa fa-user"></i></label>
                    <?php echo $this->Form->input('User.username', array( "class" =>"form-control input-lg", 'placeholder' => "E-mail de recuperação da senha", "div" => false, "label" => false, "autofocus", 'title' => "E-mail de recuperação de senha", 'required' => true, 'type' => "email" )); ?>
                </div>
                <div class="form-group">

                    <div class="col-md-12">
                        <div class="controls">

                            <input type="hidden" name="model" value="login">
                            <button type="submit" id="btn-login" class="btn btn-danger btn-md">Confirmar</button>

                        </div>
                    </div>
                </div>    

                <?php echo $this->Form->end(); ?>     
            </div>
            <div class="panel-footer" >
                <div class="row resetar-senha">
                    <div class="col-md-12">
                        <small class='text-danger'>Uma nova senha será gerada e enviada ao e-mail informado.</small>
                    </div>
                    <div class="col-md-12">
                        <a href="/users/login" title="Link para retornar à página de login">Voltar a página de login</a>
                    </div>
                </div>
            </div>                                           
        </div>  
    </div>
</div>
</div>