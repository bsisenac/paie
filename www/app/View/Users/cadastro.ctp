<br />
<div class="panel panel-info" >

	<div class="panel-heading">
		<h3>Atenção!</h3>
	</div>     


	<div class="panel-body" >
		<div class="col-md-1">
			<i class="fa fa-warning text-warning fa-4x"></i>
		</div>
		<div class="col-md-11">
			<p>
				A realização do cadastro de autenticação deste portal é feito somente pelo site de cursos e eventos do NIC.br.
			</p>
			<p>Acesse o portal <a target="_blank" title="Ir para cadastros do site cursos eventos" href="https://cursoseventos.nic.br/users/cadastro/">cursoseventos.nic.br</a>, cadastre-se, valide seu e-mail e então utilize as mesmas credenciais para realização do login deste portal.</p>
		</div>
	</div>
	<div class="panel-footer" >
		<small class="text-danger">Obs.: É obrigatório validar o cadastro para realizar o login de acesso.</small>
	</div>                                           
</div>