 <div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign In</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create( 'User', array(
                        'url' => array(
                            'controller' => 'users',
                            'action' => 'login',
                            ),
                        'class' => 'form-horizontal',
                        'role' => 'form'

                        )); ?>

                        <div class="input-group usuario">
                            <label class="input-group-addon" title="Usuário"><i class="fa fa-user"></i></label>
                            <?php echo $this->Form->input('User.username', array( "class" =>"form-control input-lg", 'placeholder' => "Usuário de acesso", "div" => false, "label" => false, "autofocus", 'title' => "Usuário de acesso" )); ?>
                        </div>
                        <br>
                        <div class="input-group senha">
                            <label class="input-group-addon" title="Senha de acesso"><i class="fa fa-unlock-alt"></i></label>
                            <?php echo $this->Form->input('User.password', array( "class" =>"form-control input-lg", 'placeholder' => "Senha de acesso", "div" => false, "label" => false, 'title' => "Senha de acesso" )); ?>
                        </div>

                        <br>
                        <div class="form-group">

                            <div class="col-md-12">
                                <div class="controls">

                                    <input type="hidden" name="model" value="login">
                                    <button type="submit" id="btn-login" class="btn btn-lg btn-success btn-block">Entrar</button>

                                </div>
                            </div>
                        </div>    
                        <?php echo $this->Form->input('User.redirect', array( 'type' => "hidden", 'value' => $redirect ) ); ?>

                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
 