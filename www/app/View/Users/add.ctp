<div class="escolas form row">
    <h3 class="page-header">Cadastrar Usuário</h3>
<?php echo $this->Form->create('User', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('grupo_id', array('class' => 'form-control'));
		echo $this->Form->input('escola_id', array('class' => 'form-control'));
		echo $this->Form->input('nome', array('class' => 'form-control'));
		echo $this->Form->input('sobrenome', array('class' => 'form-control'));
		echo $this->Form->input('password', array('class' => 'form-control'));
		echo $this->Form->input('username', array('class' => 'form-control', 'type'=>"email"));
		echo $this->Form->input('rg', array('class' => 'form-control'));
		echo $this->Form->input('cpf', array('class' => 'form-control'));
		echo $this->Form->input('cnh', array('class' => 'form-control'));
		echo $this->Form->input('vencimento_cnh', array('class' => 'form-control'));
		echo $this->Form->input('data_nascimento', array('class' => 'form-control'));
		echo $this->Form->input('telefone', array('class' => 'form-control'));
		echo $this->Form->input('logradouro_endereco', array('class' => 'form-control'));
		echo $this->Form->input('numero_endereco', array('class' => 'form-control'));
		echo $this->Form->input('cep', array('class' => 'form-control'));
		echo $this->Form->input('email', array('class' => 'form-control'));
		echo $this->Form->input('estuda_periodo', array('class' => 'form-control'));
		echo $this->Form->input('hora_busca', array('class' => 'form-control'));
		echo $this->Form->input('hora_entrega', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>