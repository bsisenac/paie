            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Usuários <a class="btn btn-sm btn-primary" href="/users/add">Cadastrar usuário</a></h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista de Usuários do Sistema
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-users">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('grupo_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('username'); ?></th>
                                        <th><?php echo $this->Paginator->sort('nome'); ?></th>
                                        <th class="actions"><?php echo __('ver'); ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user): ?>
                                        <tr>
                                            <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                                            <td>
                                                <?php echo $this->Html->link($user['Grupo']['name'], array('controller' => 'grupos', 'action' => 'view', $user['Grupo']['id'])); ?>
                                            </td>
                                            <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                                            <td><?php echo h($user['User']['nome']); ?>&nbsp;</td>
                                            <td class="actions">
                                                <?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
            $('#dataTables-users').DataTable({
                responsive: true
            });
        });
        </script>