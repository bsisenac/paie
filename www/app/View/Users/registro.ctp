<style>

	#login .form-group, #login .input-group{
		margin-top: 15px;
		margin-bottom: 25px;
	}

	#login {
		margin-bottom: 25px;
		min-height: 50vh;
		display: flex;
		align-items: center;
	}

	.titulo-login{
		margin-top: 50px;
		border-bottom: solid 2px #000;
	}

	#login .resetar-senha a{
		color: #428bca;
	}

</style>

<div class="container">

	<div class="row">
		<div id="login" class="col-md-6 col-md-offset-3"> 

			<div class="panel panel-info" >

				<div class="panel-heading panel-form-customizado">
					<div class="row">
						<div class="col-md-12">
							<h1 class="panel-title pull-left"><i class="fa fa-group fa-2x"> Cadastrar</i></h1><br/>
						</div>
						<div class="col-md-12">
							<small class="text-info">Todos os campos abaixo são de preenchimento obrigatório.</small>
						</div>
					</div>
				</div>     
				<div class="panel-body" >
					<div class="users form">
						<?php echo $this->Form->create('User', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'col-md-12 form-horizontal')); ?>
						<div class="row">
							<?php 
								$message = $this->Session->flash(); 
								if( $message ) 
									echo preg_replace( '/(<.*class="*)(message)/', "$1alert alert-danger", $message );
							?>

							<div class="input-group nome">
	                            <label class="input-group-addon" title="Nome do usuário"><i class="fa fa-user"></i></label>
	                            <?php echo $this->Form->input('User.name', array( "class" =>"form-control input-lg", 'placeholder' => "Nome de apresentação", "div" => false, "label" => false, 'autofocus', 'title' => "Nome de apresentação", 'error' => false )); ?>
	                        </div>

	                        <div class="input-group sobrenome">
	                            <label class="input-group-addon" title="Sobrenome"><i class="fa fa-user"></i></label>
	                            <?php echo $this->Form->input('User.sobrenome', array( "class" =>"form-control input-lg", 'placeholder' => "Sobrenome", "div" => false, "label" => false, 'title' => "Sobrenome", 'error' => false )); ?>
	                        </div>


	                        <div class="input-group username">
	                            <label class="input-group-addon" title="E-mail"><i class="fa fa-envelope-o"></i></label>
	                            <?php echo $this->Form->input('User.username', array( "class" =>"form-control input-lg", 'placeholder' => "E-mail de acesso", "div" => false, "label" => false, 'title' => "E-mail de acesso", 'type' => "email", 'error' => false )); ?>
	                        </div>

	                        <div class="input-group senha">
	                            <label class="input-group-addon" title="Senha"><i class="fa fa-unlock-alt"></i></label>
	                            <?php echo $this->Form->input('User.password', array( "class" =>"form-control input-lg", 'placeholder' => "Senha de acesso", "div" => false, "label" => false, 'title' => "Senha de acesso", 'type' => "password", 'error' => false )); ?>
	                        </div>

	                        <div class="input-group confirmar-senha">
	                            <label class="input-group-addon" title="Reptir a senha"><i class="fa fa-unlock-alt"></i></label>
	                            <?php echo $this->Form->input('User.password_2', array( "class" =>"form-control input-lg", 'placeholder' => "Repetir a senha", "div" => false, "label" => false, 'title' => "Confirmação de senha", 'type' => "password", 'error' => false )); ?>
	                        </div>

	                    </div>

						<div class="row">
							<?php echo $this->Form->button( 'Salvar', array('class' => 'btn btn-success'))?>
						</div>
						<?php echo $this->Form->end(); ?>
					</div>

				</div>
				<div class="panel-footer" >
					<div class="row resetar-senha">
						<div class="col-md-12">
							Voltar para o <a href="/users/login/" title="Link para a realização do login">login!</a>
						</div>
					</div>
				</div>                                           
			</div>  
		</div>
	</div>
</div>