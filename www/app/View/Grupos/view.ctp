<div class="grupos view row">
	<h2><?php echo __('Grupo'); ?> # <?php echo h($grupo['Grupo']['name']); ?></h2>
	
	<table class='table table-responsive table-hover table-striped'>
	<caption>Detalhes</caption>	
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td>
				<?php echo h($grupo['Grupo']['id']); ?>
				&nbsp;
			</td>	
		</tr>
		<tr>
			<td><strong><?php echo __('Name'); ?></strong></td>
			<td>
				<?php echo h($grupo['Grupo']['name']); ?>
				&nbsp;
			</td>	
		</tr>
		<tr>
			<td><strong><?php echo __('Dt. Criação'); ?></strong></td>
			<td>
				<?php echo h($grupo['Grupo']['created']); ?>
				&nbsp;
			</td>	
		</tr>
		<tr>
			<td><strong><?php echo __('Última Modificação'); ?></strong></td>
			<td>
				<?php echo h($grupo['Grupo']['modified']); ?>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar Grupo'), array('action' => 'edit', $grupo['Grupo']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar Grupo'), array('action' => 'delete', $grupo['Grupo']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $grupo['Grupo']['id'])); ?>
				&nbsp;
			</td>
		</tr>
	</table>
</div>

<div class="row">
	<div class="related">
		<?php if (!empty($grupo['User'])): ?>
			<div class="panel panel-default">
				<h3><?php echo __('Usuários deste grupo'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped table-hover table-condensed table-striped">
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Nome'); ?></th>
							<th><?php echo __('Login'); ?></th>
							<th><?php echo __('Dt. Criação'); ?></th>
							<th><?php echo __('Última Modificação'); ?></th>
							<th class="actions"><?php echo __('Opções'); ?></th>
						</tr>
						<?php foreach ($grupo['User'] as $user): ?>
							<tr>
								<td><?php echo $user['id']; ?></td>
								<td><?php echo $user['nome']; ?></td>
								<td><?php echo $user['username']; ?></td>
								<td><?php echo $user['created']; ?></td>
								<td><?php echo $user['modified']; ?></td>
								<td class="actions">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-sm btn-info')); ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		<?php endif; ?>
        
        <?php if (!empty($grupo['Log'])): ?>
			<div class="panel panel-default">
				<h3><?php echo __('Auditoria'); ?></h3>
				<div class="table-responsive">
			<table class="table table-striped table-hover table-condensed">
				<thead>
					<tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('Ação'); ?></th>
						<th><?php echo $this->Paginator->sort('Descrição'); ?></th>
						<th><?php echo $this->Paginator->sort('Dt. Log'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($logs as $item): ?>
						<tr>
							<td><?php echo h($item['Log']['id']); ?>&nbsp;</td>
							<td><?php echo h($item['Log']['acao_full']); ?>&nbsp;</td>
							<td><?php echo h($item['Log']['descricao']); ?>&nbsp;</td>
							<td><?php echo h($item['Log']['created']); ?>&nbsp;</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
			</div>
		<?php endif; ?>
        
	</div>
</div>