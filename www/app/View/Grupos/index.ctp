            <div class="row">
            	<div class="col-lg-12">
            		<h2 class="page-header">Grupos <a class="btn btn-sm btn-primary" href="/grupos/add">Cadastrar grupo</a></h2>
            	</div>
            	<!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
            		<div class="panel panel-default">
            			<div class="panel-heading">
            				Lista de Grupos do Sistema
            			</div>
            			<!-- /.panel-heading -->
            			<div class="panel-body">
            				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-grupos">
            					<thead>
            						<tr>
            							<th><?php echo $this->Paginator->sort('id'); ?></th>
            							<th><?php echo $this->Paginator->sort('name'); ?></th>
            							<th><?php echo $this->Paginator->sort('Dt. Criação'); ?></th>
            							<th><?php echo $this->Paginator->sort('Última Modificação'); ?></th>
            							<th class="actions"><?php echo __('ver'); ?></th>

            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach ($grupos as $grupo): ?>
            						<tr>
            							<td><?php echo h($grupo['Grupo']['id']); ?>&nbsp;</td>
            							<td><?php echo h($grupo['Grupo']['name']); ?>&nbsp;</td>
            							<td><?php echo h($grupo['Grupo']['created']); ?>&nbsp;</td>
            							<td><?php echo h($grupo['Grupo']['modified']); ?>&nbsp;</td>
            							<td class="actions">
            								<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $grupo['Grupo']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
            							</td>
            						</tr>
            					<?php endforeach; ?>

            				</tbody>
            			</table>
            			<!-- /.table-responsive -->
            		</div>
            		<!-- /.panel-body -->
            	</div>
            	<!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
        	$('#dataTables-grupos').DataTable({
        		responsive: true
        	});
        });
        </script>