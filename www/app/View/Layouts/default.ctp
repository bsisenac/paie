<!DOCTYPE html>
<html lang="pt-br">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>PAIE | <?php echo $this->fetch('title'); ?></title>

	<!-- Bootstrap Core CSS -->
	<link href="/template/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href="/template/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="/template/dist/css/sb-admin-2.css" rel="stylesheet">

	<!-- Morris Charts CSS -->
	<link href="/template/vendor/morrisjs/morris.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="/template/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="/template/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/template/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="/template/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/template/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/template/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/template/vendor/raphael/raphael.min.js"></script>
    <script src="/template/vendor/morrisjs/morris.min.js"></script>
    <script src="/template/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/template/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="/template/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/template/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/template/vendor/datatables-responsive/dataTables.responsive.js"></script>

    </head>

    <body>
    	<div id="wrapper">
    		<?php if(!empty($profile['User']['id'])): ?>

        		<?php echo $this->element('menu'); ?>

                <div id="page-wrapper">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>
               </div>

        	<?php else: ?>
    
                <?php echo $this->Session->flash(); ?>
        	   <?php echo $this->fetch('content'); ?>
               
            <?php endif; ?>
        </div>
        <!-- /#wrapper -->
    </body>
</html>