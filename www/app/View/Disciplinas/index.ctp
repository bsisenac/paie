            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Disciplinas <a class="btn btn-sm btn-primary" href="/disciplinas/add">Cadastrar disciplina</a></h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lista de Disciplinas do Sistema
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-disciplinas">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
										<th><?php echo $this->Paginator->sort('nome'); ?></th>
										<th><?php echo $this->Paginator->sort('codigo'); ?></th>
                                        <th class="actions"><?php echo __('ver'); ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($disciplinas as $disciplina): ?>
										<tr>
											<td><?php echo h($disciplina['Disciplina']['id']); ?>&nbsp;</td>
											<td><?php echo h($disciplina['Disciplina']['nome']); ?>&nbsp;</td>
											<td><?php echo h($disciplina['Disciplina']['codigo']); ?>&nbsp;</td>
											<td class="actions">
												<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $disciplina['Disciplina']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
            $('#dataTables-disciplinas').DataTable({
                responsive: true
            });
        });
        </script>