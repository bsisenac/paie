<div class="escolas form row">
    <h3 class="page-header">Editar Disciplina</h3>
<?php echo $this->Form->create('Disciplina', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('nome', array('class' => 'form-control'));
		echo $this->Form->input('codigo', array('class' => 'form-control'));
		echo $this->Form->input('Turma');
		echo $this->Form->input('User');
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>