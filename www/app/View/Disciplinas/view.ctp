<div class="disciplinas view row">
	<h2><?php echo __('Disciplina'); ?> # <?php echo h($disciplina['Disciplina']['nome']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($disciplina['Disciplina']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Nome'); ?></strong></td>
			<td><?php echo h($disciplina['Disciplina']['nome']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Codigo'); ?></strong></td>
			<td><?php echo h($disciplina['Disciplina']['codigo']); ?></td>
		</tr>
		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar disciplina'), array('action' => 'edit', $disciplina['Disciplina']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar disciplina'), array('action' => 'delete', $disciplina['Disciplina']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $disciplina['Disciplina']['id'])); ?>
			</td>
		</tr>
	</table>
</div>

<div class="row">
	<div class="related">

		<?php if (!empty($disciplina['Turma'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Turmas'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Nome'); ?></th>
						<th><?php echo __('Codigo'); ?></th>
						<th><?php echo __('Periodo'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($disciplina['Turma'] as $turma): ?>
					<tr>
						<td><?php echo $turma['id']; ?></td>
						<td><?php echo $turma['nome']; ?></td>
						<td><?php echo $turma['codigo']; ?></td>
						<td><?php echo $turma['periodo']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Ver'), array('controller' => 'turmas', 'action' => 'view', $turma['id']), array('class' => 'btn btn-sm btn-info')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
<div class="row">
	<div class="related">

		<?php if (!empty($disciplina['User'])): ?>
		<div class="panel panel-default">
			<h3><?php echo __('Users'); ?></h3>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Grupo Id'); ?></th>
						<th><?php echo __('Username'); ?></th>
						<th><?php echo __('Nome'); ?></th>
						<th><?php echo __('Sobrenome'); ?></th>
						<th><?php echo __('Password'); ?></th>
						<th><?php echo __('Created'); ?></th>
						<th><?php echo __('Modified'); ?></th>
						<th class="actions"><?php echo __('Opções'); ?></th>
					</tr>
					<?php foreach ($disciplina['User'] as $user): ?>
					<tr>
						<td><?php echo $user['id']; ?></td>
						<td><?php echo $user['grupo_id']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['nome']; ?></td>
						<td><?php echo $user['sobrenome']; ?></td>
						<td><?php echo $user['password']; ?></td>
						<td><?php echo $user['created']; ?></td>
						<td><?php echo $user['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-sm btn-info')); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
<?php endif; ?>
</div>
</div>
