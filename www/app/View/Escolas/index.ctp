            <div class="row">
            	<div class="col-lg-12">
            		<h2 class="page-header">Escolas <a class="btn btn-sm btn-primary" href="/escolas/add">Cadastrar escola</a></h2>
            	</div>
            	<!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-lg-12">
            		<div class="panel panel-default">
            			<div class="panel-heading">
            				Lista de Escolas do Sistema
            			</div>
            			<!-- /.panel-heading -->
            			<div class="panel-body">
            				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-escolas">
            					<thead>
            						<tr>
            							<th><?php echo $this->Paginator->sort('id'); ?></th>
            							<th><?php echo $this->Paginator->sort('nome'); ?></th>
            							<th><?php echo $this->Paginator->sort('cnpj'); ?></th>
            							<th><?php echo $this->Paginator->sort('endereco'); ?></th>
            							<th class="actions"><?php echo __('ver'); ?></th>

            						</tr>
            					</thead>
            					<tbody>
            						<?php foreach ($escolas as $escola): ?>
		        						<tr>
		        							<td><?php echo h($escola['Escola']['id']); ?>&nbsp;</td>
		        							<td><?php echo h($escola['Escola']['nome']); ?>&nbsp;</td>
		        							<td><?php echo h($escola['Escola']['cnpj']); ?>&nbsp;</td>
		        							<td><?php echo h($escola['Escola']['endereco']); ?>&nbsp;</td>
		        							<td class="actions">
		        								<?php echo $this->Html->link(__('Detalhes'), array('action' => 'view', $escola['Escola']['id']), array('class' => 'btn btn-sm btn-info btn-block')); ?>
		        							</td>
		        						</tr>
        							<?php endforeach; ?>
            				</tbody>
            			</table>
            			<!-- /.table-responsive -->
            		</div>
            		<!-- /.panel-body -->
            	</div>
            	<!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
        $(document).ready(function() {
        	$('#dataTables-escolas').DataTable({
        		responsive: true
        	});
        });
        </script>