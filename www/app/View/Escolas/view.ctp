<div class="escolas view row">
	<h2><?php echo __('Escola'); ?> # <?php echo h($escola['Escola']['nome']); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><strong><?php echo __('Id'); ?></strong></td>
			<td><?php echo h($escola['Escola']['id']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Nome'); ?></strong></td>
			<td><?php echo h($escola['Escola']['nome']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Cnpj'); ?></strong></td>
			<td><?php echo h($escola['Escola']['cnpj']); ?></td>
		</tr>

		<tr>
			<td><strong><?php echo __('Endereco'); ?></strong></td>
			<td><?php echo h($escola['Escola']['endereco']); ?></td>
		</tr>
		<tr>
			<td><strong><?php echo __('Opções'); ?></strong></td>
			<td>
				<?php echo $this->Html->link(__('Editar Escola'), array('action' => 'edit', $escola['Escola']['id']), array('class' => 'btn btn-sm btn-warning')); ?>
				<?php echo $this->Form->postLink(__('Deletar Escola'), array('action' => 'delete', $escola['Escola']['id']), array('class' => 'btn btn-sm btn-danger'), __('Are you sure you want to delete # %s?', $escola['Escola']['id'])); ?>
			</td>
		</tr>
	</table>
</div>

<div class="row">
	<div class="related">
		<?php if (!empty($escola['User'])): ?>
			<div class="panel panel-default">
				<h3><?php echo __('Alunos'); ?></h3>
				<div class="table-responsive">
					<table class="table table-striped table-hover table-condensed">
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Grupo'); ?></th>
							<th><?php echo __('Nome'); ?></th>
							<th><?php echo __('Sobrenome'); ?></th>
							<th><?php echo __('Rg'); ?></th>
							<th><?php echo __('Cpf'); ?></th>
							<th><?php echo __('Telefone'); ?></th>
							<th><?php echo __('Endereco'); ?></th>
							<th><?php echo __('Estuda Periodo'); ?></th>
							<th><?php echo __('Hora Busca'); ?></th>
							<th><?php echo __('Hora Entrega'); ?></th>
							<th class="actions"><?php echo __('Opções'); ?></th>
						</tr>
						<?php foreach ($escola['User'] as $user): ?>
						<tr>
							<td><?php echo $user['id']; ?></td>
							<td><?php echo $user['Grupo']['name']; ?></td>
							<td><?php echo $user['nome'] . ' ' . $user['sobrenome']; ?></td>
							<td><?php echo $user['rg']; ?></td>
							<td><?php echo $user['cpf']; ?></td>
							<td><?php echo $user['data_nascimento']; ?></td>
							<td><?php echo $user['telefone']; ?></td>
							<td><?php echo $user['logradouro_endereco'] . ', ' . $user['numero_endereco'] . ' | ' . $user['cep']; ?></td>
							<td><?php echo $user['estuda_periodo']; ?></td>
							<td><?php echo $user['hora_busca']; ?></td>
							<td><?php echo $user['hora_entrega']; ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-sm btn-info')); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</table>
				</div>
			</div>
		<?php endif; ?>

</div>
</div>
