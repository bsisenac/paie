<div class="escolas form row">
    <h3 class="page-header">Cadastrar Escola</h3>
<?php echo $this->Form->create('Escola', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('nome', array('class' => 'form-control'));
		echo $this->Form->input('cnpj', array('class' => 'form-control'));
		echo $this->Form->input('endereco', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
