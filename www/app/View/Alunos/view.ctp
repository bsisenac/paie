<div class="alunos view">
	<h2><?php echo __('Aluno'); ?></h2>
	<table class='table table-responsive table-hover table-striped'>
		<caption>Detalhes</caption>
		<tr>
			<td><?php echo __('Id'); ?></td>
			<td><?php echo h($aluno['Aluno']['id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('User Aluno Id'); ?></td>
			<td><?php echo h($aluno['Aluno']['user_aluno_id']); ?></td>
		</tr>

		<tr>
			<td><?php echo __('User Pai Id'); ?></td>
			<td><?php echo h($aluno['Aluno']['user_pai_id']); ?></td>
		</tr>
	</table>
</div>
<div class="actions">
	<div class="row">
		<h3><?php echo __('Opções'); ?></h3>

				<?php echo $this->Html->link(__('Editar Aluno'), array('action' => 'edit', $aluno['Aluno']['id']), array('class' => 'btn btn-primary')); ?>
		<?php echo $this->Form->postLink(__('Deletar Aluno'), array('action' => 'delete', $aluno['Aluno']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $aluno['Aluno']['id'])); ?>
		<?php echo $this->Html->link(__('Ver Alunos'), array('action' => 'index'), array('class' => 'btn btn-warning')); ?>
		<?php echo $this->Html->link(__('Adicionar Aluno'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
	</div>
	<div class="row">
		<h3><?php echo __('Itens Relacionados'); ?></h3>
			</div>
</div>
