<div class="alunos form">
	<legend><?php echo __('Edit Aluno'); ?></legend>
<?php echo $this->Form->create('Aluno', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'), 'class' => 'well form-horizontal')); ?>
	<fieldset>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('user_aluno_id', array('class' => 'form-control'));
		echo $this->Form->input('user_pai_id', array('class' => 'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->button('Salvar', array('class' => 'btn btn-success'))?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<h3><?php echo __('Opções'); ?></h3>

<div class="row">
		<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $this->Form->value('Aluno.id')), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $this->Form->value('Aluno.id'))); ?>		<?php echo $this->Html->link(__('Ver Alunos'), array('action' => 'index'), array('class' => 'btn btn-default')); ?>		</div>
</div>
