create table escolas (
	id int not null primary key auto_increment,
	nome varchar(255) not null,
	cnpj varchar(255) not null,
	endereco varchar(255) not null
);

create table turmas (
	id int not null primary key auto_increment,
	nome varchar(255) not null,
	codigo varchar(255) not null,
	periodo varchar(255) not null,
	escola_id int not null
);

create table disciplinas (
	id int not null primary key auto_increment,
	nome varchar(255) not null,
	codigo varchar(255) not null
);

create table turmas_disciplinas (
	id int not null primary key auto_increment,
	disciplina_id int not null,
	turma_id int not null
);

create table users_disciplinas (
	id int not null primary key auto_increment,
	user_id int not null,
	disciplina_id int not null
);

create table users_turmas (
	id int not null primary key auto_increment,
	user_id int not null,
	turma_id int not null
);

create table alunos (
	id int not null primary key auto_increment,
	user_id_aluno int not null,
	user_id_pai int not null
);

create table notificacoes (
	id int not null primary key auto_increment,
	texto int not null,
	user_id int not null
);

create table notas (
	id int not null primary key auto_increment,
	nota int not null,
	disciplina_id int not null,
	user_id int not null,
	created datetime,
	modified datetime
);

create table escolas (
	id int not null primary key auto_increment,
	nome varchar(255) not null,
	cnpj varchar(255) not null,
	endereco varchar(255) not null
);

create table transportes (
	id int not null primary key auto_increment,
	marca_veiculo varchar(255) not null,
	placa_veiculo varchar(255) not null,
	ano_fabricacao_veiculo varchar(255) not null,
	documentacao_veiculo tinyint(1),
	documentacao_pendente varchar(255),
	total_quantidade_vagas int not null,
	quantidade_vagas_atual int not null
);
create table transportes_users (
	id int not null primary key auto_increment,
	user_id int not null,
	transporte_id int not null,
	relacao_transporte varchar(255) not null
);
alter table users add column rg varchar(255) not null;
alter table users add column cpf varchar(255) not null;
alter table users add column cnh varchar(255) not null;
alter table users add column vencimento_cnh varchar(255) not null;
alter table users add column data_nascimento varchar(255) not null;
alter table users add column telefone varchar(255) not null;
alter table users add column logradouro_endereco varchar(255) not null;
alter table users add column numero_endereco varchar(255) not null;
alter table users add column cep varchar(255) not null;
alter table users add column email varchar(255);
alter table users add column estuda_periodo varchar(255);
alter table users add column hora_busca time;
alter table users add column hora_entrega time;
alter table users add column escola_id int;
